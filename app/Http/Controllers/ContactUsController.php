<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class ContactUsController extends Controller
{
    public function index()
    {

        $pages = Page::All();

        return view('website.contactUs', ['pages' => $pages]);
    }

    public function sendMessage(Request $req)
    {

        $input = $req->all();
        $pages = Page::All();

        $validator = $req->validate([
            'name' => 'required',
            'email' => 'required|email',
        ]);

        Mail::send(
            'mails.contacts',
            ['nameInput' => $input['name'], 'messageInput' => $input['message']],
            function ($msg) use ($input) {
                $msg->from('asadsohailabid@gmail.com', 'LARAVEL PROJECT');

                $msg->to($input['email']);
            }
        );

        // \Mail::to('asadsohailabid@gmail.com')->send(new \App\Mail\ContactMail($input));

        return view('website.contactUs', ['pages' => $pages])->with('successMessage', 'Thank You For Your Message');
    }

    public function sendMessageAjax(Request $req)
    {
        $input = $req->all();

        $validator = $req->validate([
            'name' => 'required',
            'email' => 'required|email',
        ]);

        Mail::send(
            'mails.contacts',
            ['nameInput' => $input['name'], 'messageInput' => $input['message']],
            function ($msg) use ($input) {
                $msg->from('asadsohailabid@gmail.com', 'LARAVEL PROJECT');

                $msg->to($input['email']);
            }
        );

        return [
            'success' => true,
            'message' => 'Thank You! We will get back to you ASAP!'
        ];
    }
}
