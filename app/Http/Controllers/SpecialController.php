<?php

namespace App\Http\Controllers;

use App\Special;
use Illuminate\Http\Request;

class SpecialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specials = Special::All();
        return view('website.admin.specials.index', ['specials' => $specials]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('website.admin.specials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        $special = new Special();

        $special->name = $inputs['name'];
        $special->description = $inputs['description'];
        $special->was_price = $inputs['was_price'];
        $special->current_price = $inputs['current_price'];

        $special->save();

        return redirect('/admin/specials');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $special = Special::where('id', $id)->first();

        return view('website.admin.specials.edit', ['special' => $special]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->all();

        $special = Special::where('id', $id)->first();

        $special->name = $inputs['name'];
        $special->description = $inputs['description'];
        $special->was_price = $inputs['was_price'];
        $special->current_price = $inputs['current_price'];

        $special->save();

        return redirect('/admin/specials');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Special::where('id', $id)->delete();

        return redirect('/admin/specials');
    }
}
