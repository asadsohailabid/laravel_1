require('./bootstrap.js');


window.Vue = require('vue');

Vue.component('contact-us-form', require('./components/ContactUsForm.vue').default);

const app = new Vue({
    el: '#app',
});