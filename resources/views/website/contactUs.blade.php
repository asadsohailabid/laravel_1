<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>

    <link rel="stlyesheet" href="/css/website.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>

<body>
    <div id="app">
        <header class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a href="{{ URL::to('/') }}" class="navbar-brand font-weight-bold">Laravel Project</a>
                <ul class="navbar-nav">
                    @foreach ($pages as $page)
                        <li class="nav-item"><a class="nav-link" href="/page/{{ $page->id }}">{{ $page->name }}</a></li>
                    @endforeach
                    <li class="nav-item"><a class="nav-link" href="/contact-us">Contact Us</a></li>
                </ul>
            </div>
        </header>
        <contact-us-form></contact-us-form>
        {{-- <div class="container">
            <form class="form-horizontal" method="post" action="contact-us/sendMessage" role="form"
                style="margin-left:auto;margin-right:auto;margin-top:25px;">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (isset($successMessage))
                    <div class="alert alert-success">
                        <p>{{ $successMessage }}</p>
                    </div>
                @endif

                @csrf
                <div class="form-group row">
                    <label for="name" class="col-md-3 text-center">Name:</label>
                    <input type="text" class="col-md-8 form-control" placeholder="Enter Name" id="name" name="name" />
                </div>
                <div class="form-group row">
                    <label for="email" class="col-md-3 text-center">Email:</label>
                    <input type="email" class="col-md-8 form-control" placeholder="Enter Email" id="email"
                        name="email" />
                </div>
                <div class="form-group row">
                    <label for="email" class="col-md-3 text-center">Message:</label>
                    <textarea type="message" class="col-md-8 form-control" placeholder="Enter Message" id="message"
                        name="message" style="resize: none;"></textarea>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-md-3 text-center"></label>
                    <button class="col-md-8 btn btn-block border border-black">Submit</button>
                </div>
            </form>
        </div> --}}
    </div>
    <!--    SCRIPT RELEVANT STUFF HERE  -->
    <div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
        </script>

        <script src="/js/website.js"></script>
    </div>
</body>

</html>
