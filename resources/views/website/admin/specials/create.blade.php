<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Special</title>

    <link rel="stlyesheet" href="/css/website.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>

<body>
    <div id="app">
        <header class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a href="{{ URL::to('/') }}" class="navbar-brand font-weight-bold">Laravel Project</a>
                <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link" href="#">Go Back</a></li>
                </ul>
            </div>
        </header>
        <div class="container text-center">
            <a class="btn btn-info text-center" href="{{ URL::to('/admin/specials') }}">Go Back</a>
            <form class="form-horizontal" method="post" action="{{ URL::to('admin/specials') }}" role="form"
                method="POST" style="margin-left:auto;margin-right:auto;margin-top:25px;">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (isset($successMessage))
                    <div class="alert alert-success">
                        <p>{{ $successMessage }}</p>
                    </div>
                @endif

                @csrf
                <div class="form-group row">
                    <label for="name" class="col-md-3 text-center">Name:</label>
                    <input type="text" class="col-md-8 form-control" placeholder="Enter Name" id="name" name="name"
                        required />
                </div>
                <div class="form-group row">
                    <label for="description" class="col-md-3 text-center">Description:</label>
                    <textarea type="text" class="col-md-8 form-control" placeholder="Enter Description" id="description"
                        name="description" style="resize: none;" required></textarea>
                </div>
                <div class="form-group row">
                    <label for="was_price" class="col-md-3 text-center">Was Price:</label>
                    <input step="0.01" type="number" class="col-md-8 form-control" placeholder="Enter Previous Price"
                        id="was_price" name="was_price" required />
                </div>
                <div class="form-group row">
                    <label for="current_price" class="col-md-3 text-center">Current Price:</label>
                    <input step="0.01" type="number" class="col-md-8 form-control" placeholder="Enter Current Price"
                        id="current_price" name="current_price" required />
                </div>
                <div class="form-group row">
                    <label for="submit" class="col-md-3 text-center"></label>
                    <button class="col-md-8 btn btn-block border border-black" id="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
    <!--    SCRIPT RELEVANT STUFF HERE  -->
    <div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
        </script>

        <script src="/js/website.js"></script>
    </div>
</body>

</html>
