<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Specials</title>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>

<body>
    <header class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a href="{{ URL::to('/') }}" class="navbar-brand font-weight-bold">Laravel Project</a>
            <ul class="navbar-nav">
                <li class="nav-item"><a class="nav-link" href="{{ URL::to('/') }}">Go Back</a></li>
            </ul>
        </div>
    </header>
    <div class="container text-center">
        <a class="btn btn-info text-center" href="{{ URL::to('/admin/specials/create') }}">Add Special</a>
        <table class="table">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Old Price</th>
                    <th>New Price</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @php
                $i = 1;
                @endphp
                @foreach ($specials as $special)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $special->name }}</td>
                        <td>{{ $special->description }}</td>
                        <td>{{ $special->was_price }}</td>
                        <td>{{ $special->current_price }}</td>
                        <td><a href="{{ URL::to('/admin/specials/' . $special->id . '/edit') }}"
                                class="btn btn-sm border-info">Edit</a></td>
                        <td>
                            <form method="post" action="{{ URL::to('admin/specials/'.$special->id) }}" role="form" method="POST">
                                @method('delete')
                                @csrf
                                <button class="btn btn-sm border-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>

</html>
